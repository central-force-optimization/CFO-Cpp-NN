#include <iostream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "tnt.h"
#include "hr_time.h"
#include "MersenneTwister.h"
#include <fstream>
#include <sstream>
#include <limits>


using namespace std;
using namespace TNT;

// Constants
static const long double Pi = 3.141592653589793238462643;
static const long double TwoPi = 2 * Pi;
static const long double e = 2.718281828459045235360287;

long double sigmoid(long double x){
	return 1/(1+exp(-x));
}
long double XORNN(Array3D<long double> R, int Nd, int p, int j) {
	long double Z=0,  Xi;
	long double n1,n2,n3,x1,x2,x3, e = 0.0, out;

	Array2D<long double> inputs(4,2);
	Array1D<long double> outputs(4);

	inputs[0][0] = 0;inputs[0][1] = 0;outputs[0] = 0;
	inputs[1][0] = 0;inputs[1][1] = 1;outputs[1] = 1;
	inputs[2][0] = 1;inputs[2][1] = 0;outputs[2] = 1;
	inputs[3][0] = 1;inputs[3][1] = 1;outputs[3] = 0;

	//9 dimensions
	for(int x=0; x<4; x++){
		x1 = inputs[x][0]; x2 = inputs[x][1]; out = outputs[x];
		n1 = sigmoid(R[p][0][j]*x1 + R[p][1][j]*x2);
		n2 = sigmoid(R[p][2][j]*x1 + R[p][3][j]*x2);
		n3 = sigmoid(R[p][4][j]*x1 + R[p][5][j]*x2);

		out =   R[p][6][j]*n1 +  R[p][7][j]*n2 +  R[p][8][j]*n3;

		e = e + pow(fabs(e-out),2);
	}

	Z = e/4;
	return -Z;
}
long double UnitStep(long double X) {

	if (X < 0.0)
		return 0.0;

	return 1.0;
}
bool loaded = false;
vector< vector<long double > > testData;
vector< vector<long double > > trainData;
long double IrisNN(Array3D<long double> R, int Nd, int p, int j) {

	long double Z, e=0.0, x1, x2,x3,x4,out1, out2, out3, n1, n2, n3, n4, n5;
	MTRand mt;

	if(!loaded ){
		ifstream myFile;
		myFile.open("../data/irisData");
		if (myFile.is_open()) {
			for(int x=0; x<150; x++){
				vector<long double> v(5,0);

				myFile >> v[0];
				myFile >> v[1];
				myFile >> v[2];
				myFile >> v[3];
				if(x < 50) 		{ v[4] = 1; }
				else if(x < 100){ v[4] = 2; }
				else 			{ v[4] = 3;	}
				
				if(mt.rand() > .33) { trainData.push_back(v);}
				else 				{ testData.push_back(v); }
			}
			loaded = true;
			myFile.close();
		}
	}

	for(int x=0; x<trainData.size(); x++){

		x1 = trainData[x][0]; x2 = trainData[x][1]; x3 = trainData[x][2]; x4 = trainData[x][3];

		n1 = sigmoid(R[p][0][j]*x1 + R[p][1][j]*x2 + R[p][2][j]*x3 + R[p][3][j]*x4);
		n2 = sigmoid(R[p][4][j]*x1 + R[p][5][j]*x2 + R[p][6][j]*x3 + R[p][7][j]*x4);
		n3 = sigmoid(R[p][8][j]*x1 + R[p][9][j]*x2 + R[p][10][j]*x3 + R[p][11][j]*x4);
		out1 = sigmoid(R[p][12][j]*n1 +  R[p][13][j]*n2 +  R[p][14][j]*n3);
		out2 = sigmoid(R[p][15][j]*n1 +  R[p][16][j]*n2 +  R[p][17][j]*n3);
		out3 = sigmoid(R[p][18][j]*n1 +  R[p][19][j]*n2 +  R[p][20][j]*n3);


		if(trainData[x][4] == 1)		{e = e + pow((1-out1) + (0-out2) + (0-out3),2);}
		else if(trainData[x][4] == 2)	{e = e + pow((0-out1) + (1-out2) + (0-out3),2);}
		else							{e = e + pow((0-out1) + (0-out2) + (1-out3),2);}

	}

	Z = -e/(double)trainData.size();
	return Z;
}
long double IrisNN2(Array3D<long double> R, int Nd, int p, int j) {

	long double Z, e=0.0, x1, x2,x3,x4,out1, out2, out3, n1, n2, n3, n4, n5;
	MTRand mt;

	if(!loaded ){
		ifstream myFile;
		myFile.open("../data/irisData");
		if (myFile.is_open()) {
			for(int x=0; x<150; x++){
				vector<long double> v(5,0);

				myFile >> v[0];
				myFile >> v[1];
				myFile >> v[2];
				myFile >> v[3];
				if(x < 50) 		{ v[4] = 1; }
				else if(x < 100){ v[4] = 2; }
				else 			{ v[4] = 3;	}
				
				if(mt.rand() > .33) { trainData.push_back(v);}
				else 				{ testData.push_back(v); }
			}
			loaded = true;
			myFile.close();
		}
	}

	for(int x=0; x<trainData.size(); x++){

		x1 = trainData[x][0]; x2 = trainData[x][1]; x3 = trainData[x][2]; x4 = trainData[x][3];


		n1 = sigmoid(R[p][0][j]*x1 + R[p][1][j]*x2 + R[p][2][j]*x3 + R[p][3][j]*x4);
		n2 = sigmoid(R[p][4][j]*x1 + R[p][5][j]*x2 + R[p][6][j]*x3 + R[p][7][j]*x4);
		n3 = sigmoid(R[p][8][j]*x1 + R[p][9][j]*x2 + R[p][10][j]*x3 + R[p][11][j]*x4);
		n4 = sigmoid(R[p][12][j]*x1 + R[p][13][j]*x2 + R[p][14][j]*x3 + R[p][15][j]*x4);
		n5 = sigmoid(R[p][16][j]*x1 + R[p][17][j]*x2 + R[p][18][j]*x3 + R[p][19][j]*x4);

		out1 = sigmoid(R[p][20][j]*n1 +  R[p][21][j]*n2 +  R[p][22][j]*n3 +  R[p][23][j]*n4 +  R[p][24][j]*n5);
		out2 = sigmoid(R[p][25][j]*n1 +  R[p][26][j]*n2 +  R[p][27][j]*n3 +  R[p][28][j]*n4 +  R[p][29][j]*n5);
		out3 = sigmoid(R[p][30][j]*n1 +  R[p][31][j]*n2 +  R[p][32][j]*n3 +  R[p][33][j]*n4 +  R[p][34][j]*n5);

		if(trainData[x][4] == 1)		{e = e + pow((1-out1) + (0-out2) + (0-out3),2);}
		else if(trainData[x][4] == 2)	{e = e + pow((0-out1) + (1-out2) + (0-out3),2);}
		else							{e = e + pow((0-out1) + (0-out2) + (1-out3),2);}
	}

	Z = -e/(double)trainData.size();
	return Z;
}
bool hasFitnessSaturated(int nSteps, int j, int Np, int Nd,	Array2D<long double> M, Array3D<long double> R, int DiagLength) {

	long double fitnessSatTol = 0.000001;
	long double bestFitness = -INFINITY;
	long double bestFitnessStepJ = -INFINITY;

	if (j < nSteps + 10)
		return false;

	long double sumOfBestFitness = 0;

	for (int k = j - nSteps + 1; k <= j; k++) {
		bestFitness = -INFINITY;

		for (int p = 0; p < Np; p++) {

			if (M[p][k] >= bestFitness) {
				bestFitness = M[p][k];
			}
		}

		if(k == j){
			bestFitnessStepJ = bestFitness;
		}

		sumOfBestFitness += bestFitness;
	}

	if (fabs(sumOfBestFitness / nSteps - bestFitnessStepJ) <= fitnessSatTol) {
		return true;
	}

	return false;
}

void GetBestFitness(Array2D<long double> M, int Np, int stepNumber, long double& bestFitness, int& bestProbeNumber, int& bestTimeStep){

	bestFitness = M[0][0];

	for(int i=0; i<stepNumber; i++){
		for(int p=0; p<Np; p++){
			if(M[p][i] >= bestFitness){
				bestFitness = M[p][i];
				bestProbeNumber = p;
				bestTimeStep = i;
			}
		}
	}
}

int getMaxProbes(int i){

	if(i >= 1 && i <= 6){
		return 14;
	}else if(i >= 7 && i <= 10){
		return 12;
	}else if(i >= 11 && i <= 15){
		return 10;
	}if(i >= 16 && i <= 20){
		return 8;
	}if(i >= 21 && i <= 30){
		return 6;
	}

	return 4;

}

void MiniBenchmarkCFO(int& bestNp, int Nd, int& Nt, long double& bestGamma, long double oMin, long double oMax, long double(*objFunc)(Array3D<long double> ,int, int, int), string functionName, int IPD) {

	vector<long double> xMin;
	vector<long double> xMax;
	long double bestFitness = -INFINITY;
	long double bestFitnessThisRun = -INFINITY;
	long double bestFitnessOverall = -INFINITY;
	long double Gamma, Frep;

	long double deltaFrep = 0.1;
	long double DeltaXi;
	long double bestCoords[Nd];
	long double Denom, Numerator, SumSQ, DiagLength = 0;
	long double Alpha = 1, Beta = 2;
	Array3D<long double> bestR;//(Np, Nd, Nt);
	Array3D<long double> bestA;//(Np, Nd, Nt);
	Array2D<long double> bestM;//(Np, Nt);
	string s;
	stringstream ss;
	ofstream myFile;

	int lastStep, Np, numEvals = 0;
	int bestProbeNumber = 0, bestTimeStep = 0;
	int bestProbeNumberThisRun = 0, bestTimeStepThisRun = 0;
	int bestProbeNumberOverall = 0, bestTimeStepOverall = 0;
	int maxProbesPerDimension = getMaxProbes(Nd);
	long double NumGammas = 21;
	int bestNpNd;
	int	lastStepBestRun;

	string fileName;

	xMin.resize(Nd, oMin);
	xMax.resize(Nd, oMax);

	for(int numProbesPerAxis = 4; numProbesPerAxis <= maxProbesPerDimension; numProbesPerAxis += 2){
		Np = numProbesPerAxis*Nd;

		Array3D<long double> R(Np, Nd, Nt);
		Array3D<long double> A(Np, Nd, Nt);
		Array2D<long double> M(Np, Nt);

		for(int GammaNumber=1; GammaNumber <=NumGammas; GammaNumber++){
			Gamma = (GammaNumber-1)/(NumGammas-1);

			// Reset Matrices
			for(int j=0; j<Nt; j++){
				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {
						R[p][i][j] = 0;	A[p][i][j] = 0;
					}
					M[p][j] = 0;
				}
			}
			// Reset Bests
			bestFitness 	= bestFitnessThisRun 		=  -INFINITY;
			bestProbeNumber = bestProbeNumberThisRun 	= 0;
			bestTimeStep 	= bestTimeStepThisRun 		= 0;
			// End Reset Values


			for (int i = 0; i < Nd; i++) {
				DiagLength += pow(xMax[i] - xMin[i], 2);
			}
			DiagLength = sqrt(DiagLength);

			// Initial Probe Distribution
			switch(IPD){
				case 1:
					for (int i = 0; i < Nd; i++) {
						for (int p = 0; p < Np; p++) {
							R[p][i][0] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
						}
					}

					for (int i = 0; i < Nd; i++) {
						DeltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
						int p;

						for (int k = 0; k < numProbesPerAxis; k++) {
							p = k + numProbesPerAxis * i;
							R[p][i][0] = xMin[i] + k * DeltaXi;
						}
					}
					break;
				case 2:
					for(int p=0; p<Np; p++){
						for(int i=0; i<Nd; i++){
							DeltaXi = (xMax[i] -xMin[i])/(Np-1);
							R[p][i][0] = xMin[i] + p*DeltaXi;
						}
					}
					break;
				case 3:
					srand(time(NULL));
					for(int p=0; p<Np; p++){
						for(int i=0; i<Nd; i++){
							R[p][i][0] = rand() % (int)xMax[i] + xMin[i];
						}
					}
				break;
			}

			// Set Initial Acceleration to 0
			for (int p = 0; p < Np; p++) {
				for (int i = 0; i < Nd; i++) {
					A[p][i][0] = 0;
				}
			}

			// Compute Initial  Fitness
			for (int p = 0; p < Np; p++) {
				M[p][0] = objFunc(R, Nd, p, 0);
				numEvals++;
			}

			// Time Steps
			lastStep = Nt;
			bestFitnessThisRun = M[0][0];// -INFINITY;
			Frep = 0.5;

			for (int j = 1; j < Nt; j++) {
				// Compute new positions
				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {
						R[p][i][j] = R[p][i][j - 1] + A[p][i][j - 1];
					}
				}

				// Correct any errors
				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {

						if (R[p][i][j] < xMin[i]) {
							R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
						}
						if (R[p][i][j] > xMax[i]) {
							R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
						}
					}
				}
				// Update Fitness
				for (int p = 0; p < Np; p++) {
					M[p][j] = objFunc(R, Nd, p, j);
					numEvals++;
					if(M[p][j] >= bestFitness){
						bestFitness 	= M[p][j];
						bestTimeStep 	= j;
						bestProbeNumber = p;
					}
				}

				if(bestFitness >= bestFitnessThisRun){
					bestFitnessThisRun 		= bestFitness;
					bestProbeNumberThisRun 	= bestProbeNumber;
					bestTimeStepThisRun 	= bestTimeStep;
				}

				// Compute next Acceleration

				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {

						A[p][i][j] = 0;

						for (int k = 0; k < Np; k++) {
							if (k != p) {
								SumSQ = 0.0;

								for (int L = 0; L < Nd; L++) {
									SumSQ = SumSQ + pow(R[k][L][j] - R[p][L][j], 2);
								}
								if(SumSQ != 0){
									Denom = sqrt(SumSQ);
									Numerator = UnitStep((M[k][j] - M[p][j])) * (M[k][j]- M[p][j]);
									A[p][i][j] = A[p][i][j] + (R[k][i][j] - R[p][i][j]) * pow(Numerator,Alpha)/pow(Denom,Beta);
								}
							}
						}
					}
				}

				// Adjust Frep
				Frep += deltaFrep;
				if(Frep > 1.0){
					 Frep = 0.05;
				}

				// Shrink Boundaries
				if(j % 10 == 0 && j >= 20){
					for(int i=0; i<Nd; i++){
						xMin[i] = xMin[i] + (R[bestProbeNumber][i][bestTimeStep] - xMin[i])/2;
						xMax[i] = xMax[i] - (xMax[i] - R[bestProbeNumber][i][bestTimeStep])/2;
					}
					// Correct any errors
					for (int p = 0; p < Np; p++) {
						for (int i = 0; i < Nd; i++) {

							if (R[p][i][j] < xMin[i]) {
								R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
							}
							if (R[p][i][j] > xMax[i]) {
								R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
							}
						}
					}
				}

				// Test for fitness saturation
				lastStep = j;
				if(fabs(bestFitness) < 0.02)break;
				//if (hasFitnessSaturated(25, j, Np, Nd, M, R, DiagLength)) {break;}

			}//End Time Steps

			if(bestFitnessThisRun >= bestFitnessOverall){
				bestFitnessOverall 		= bestFitnessThisRun;
				bestProbeNumberOverall 	= bestProbeNumberThisRun;
				bestTimeStepOverall 	= bestTimeStepThisRun;

				bestNpNd 		= numProbesPerAxis;
				bestGamma 		= Gamma;
				lastStepBestRun = lastStep;
				bestNp			= Np;
			}

			//Reset Decision Space
			xMin.clear(); xMax.clear();
			xMin.resize(Nd, oMin);
			xMax.resize(Nd, oMax);

/*
			cout << "Function Name:        		" << functionName << endl;
			cout << "Best Fitness:    			" << setprecision(18) << bestFitness << endl;
			cout << "Best Probe #:    			" << setprecision(0) << bestProbeNumber << endl;
			cout << "Best Time Step:  			" << bestTimeStep << endl;
			cout << "Gamma:           			" << setprecision(18) <<  Gamma << endl;
			cout << "Probes Per Axis: 			" << numProbesPerAxis << endl;
			cout << "Number of Probes:			" << Np << endl;
			cout << "Last Step:       			" << setprecision(0) << lastStep << endl << endl;
*/

		}//End Gamma
	}//End NpNd
	cout << "Function Name:  		" << functionName << endl;
	cout << "Best Overall Fitness: 		" << setprecision(18) << bestFitnessOverall << endl;
	cout << "Best Overall Probe #: 		" << setprecision(0) << bestProbeNumberOverall << endl;
	cout << "Best Overall TimeStep:		" << bestTimeStepOverall << endl;
	cout << "Best Gamma:		   	" << setprecision(2) << bestGamma << endl;
	cout << "Best Probes Per Axis: 		" << setprecision(0) << bestNpNd << endl;
	cout << "Best Number Of Probes:		" << bestNp << endl;
	cout << "Overall Last Step:    		" << lastStepBestRun << endl;
	cout << "Number of Evals:       " << numEvals << endl;

	Nt = lastStepBestRun+1;
}

void PrintResults(Array3D<long double> R, Array3D<long double> A, Array2D<long double> M, Array1D<int> bestProbeNumberArray, Array1D<long double> bestFitnessArray,int Np, int Nd, int lastStep, string functionName) {
	ofstream myFile;
	string fileName = functionName + "_R" + ".txt" ;
	myFile.open(fileName.c_str(), ios::trunc);
	for(int j=0; j<lastStep; j++){
		for(int p=0; p<Np; p++){
			for(int i=0; i<Nd; i++){
				myFile << j << "," << p+1 << "," << i+1 << "," << setprecision(15) << R[p][i][j] << endl;
			}
		}
	}
	myFile.close();
	fileName = functionName +"_A" + ".txt";
	myFile.open(fileName.c_str(), ios::trunc);
	for(int j=0; j<lastStep; j++){
		for(int p=0; p<Np; p++){
			for(int i=0; i<Nd; i++){
				myFile << j << "," << p+1 << "," << i+1 << "," << A[p][i][j] << endl;
			}
		}
	}
	myFile.close();
	fileName = functionName + "_M" + ".txt";
	myFile.open(fileName.c_str(), ios::trunc);
	for(int j=0; j<lastStep; j++){
		for(int p=0; p<Np; p++){
			myFile << j << "," << p+1 << "," << M[p][j] << endl;
		}
	}
	myFile.close();
	fileName = functionName + "_Bests" + ".txt";
	myFile.open(fileName.c_str(), ios::trunc);
	for(int j=0; j<lastStep; j++){
		myFile << j << "," << bestProbeNumberArray[j] << "," << bestFitnessArray[j] << endl;
	}
	myFile.close();
}

void CFO(int Np, int Nt, int Nd, long double Alpha, long double Beta, long double Frep, long double Gamma, int& lastStep, vector<long double> xMin, vector<long double> xMax, long double(*objFunc)(Array3D<long double> ,int, int, int), string functionName, int IPD) {

	long double DeltaXi, bestFitness = -INFINITY, deltaFrep = 0.1;
	long double Denom, Numerator, SumSQ, DiagLength = 0;
	int numEvals = 0;
	Array3D<long double> R(Np, Nd, Nt);
	Array3D<long double> A(Np, Nd, Nt);
	Array2D<long double> M(Np, Nt);
	Array1D<long double> bestFitnessArray(Nt);
	Array1D<int> bestProbeNumberArray(Nt);

	int bestTimeStep = 0, bestProbeNumber = 0;
	int numProbesPerAxis = Np / Nd;

	string fileName;

	for (int i = 0; i < Nd; i++) {
		DiagLength += pow(xMax[i] - xMin[i], 2);
	}
	DiagLength = sqrt(DiagLength);

	//
	// Initial Probe Distribution
	//
	switch(IPD){
		case 1:
			for (int i = 0; i < Nd; i++) {
				for (int p = 0; p < Np; p++) {
					R[p][i][0] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
				}
			}

			for (int i = 0; i < Nd; i++) {
				DeltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
				int p;

				for (int k = 0; k < numProbesPerAxis; k++) {
					p = k + numProbesPerAxis * i;
					R[p][i][0] = xMin[i] + k * DeltaXi;
				}
			}
			break;
		case 2:
			for(int p=0; p<Np; p++){
				for(int i=0; i<Nd; i++){
					DeltaXi = (xMax[i] -xMin[i])/(Np-1);
					R[p][i][0] = xMin[i] + p*DeltaXi;
				}
			}
			break;
		case 3:
			srand(time(NULL));
			for(int p=0; p<Np; p++){
				for(int i=0; i<Nd; i++){
					R[p][i][0] = rand() % (int)xMax[i] + xMin[i];
				}
			}
		break;
	}

	//
	// Set Initial Acceleration to 0
	//
	for (int p = 0; p < Np; p++) {
		for (int i = 0; i < Nd; i++) {
			A[p][i][0] = 0;
		}
	}

	//
	// Compute Initial  Fitness
	//
	for (int p = 0; p < Np; p++) {
		M[p][0] = objFunc(R, Nd, p, 0);
		numEvals++;
	}

	//
	// Time Steps
	//
	lastStep = Nt;
	for (int j = 1; j < Nt; j++) {
		//
		// Compute new positions
		//
		for (int p = 0; p < Np; p++) {
			for (int i = 0; i < Nd; i++) {
				R[p][i][j] = R[p][i][j - 1] + A[p][i][j - 1];
			}
		}

		//
		// Correct any errors
		//
		for (int p = 0; p < Np; p++) {
			for (int i = 0; i < Nd; i++) {
				if (R[p][i][j] < xMin[i]) {
					R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
				}
				if (R[p][i][j] > xMax[i]) {
					R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
				}
			}
		}

		//
		// Update Fitness
		//
		for (int p = 0; p < Np; p++) {
			M[p][j] = objFunc(R, Nd, p, j);
			numEvals++;

			if(M[p][j] >= bestFitness){
				bestFitness 	= M[p][j];
				bestTimeStep 	= j;
				bestProbeNumber = p;
			}
		}

		//
		// Compute next Acceleration
		//
		for (int p = 0; p < Np; p++) {
			for (int i = 0; i < Nd; i++) {

				A[p][i][j] = 0;

				for (int k = 0; k < Np; k++) {
					if (k != p) {
						SumSQ = 0.0;

						for (int L = 0; L < Nd; L++) {
							SumSQ = SumSQ + pow(R[k][L][j] - R[p][L][j], 2);
						}
						if(SumSQ != 0){
							Denom = sqrt(SumSQ);
							Numerator = UnitStep((M[k][j] - M[p][j])) * (M[k][j]- M[p][j]);
							A[p][i][j] = A[p][i][j] + (R[k][i][j] - R[p][i][j]) * pow(Numerator,Alpha)/pow(Denom,Beta);
						}
					}
				}
			}
		}

		//
		// Adjust Frep
		//
		Frep += deltaFrep;
		if(Frep > 1){
			 Frep = 0.05;
		}

		//
		// Shrink Boundaries
		//
		if(j % 10 == 0 && j >= 20){
			for(int i=0; i<Nd; i++){
				xMin[i] = xMin[i] + (R[bestProbeNumber][i][bestTimeStep] - xMin[i])/2.0;
				xMax[i] = xMax[i] - (xMax[i] - R[bestProbeNumber][i][bestTimeStep])/2.0;
			}
			//
			// Correct any errors
			//
			for (int p = 0; p < Np; p++) {
				for (int i = 0; i < Nd; i++) {

					if (R[p][i][j] < xMin[i]) {
						R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
					}
					if (R[p][i][j] > xMax[i]) {
						R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
					}
				}
			}
		}

		//
		// Test for fitness saturation
		//
		lastStep = j;
		if(fabs(bestFitness) < 0.02)break;
		//if (hasFitnessSaturated(25, j, Np, Nd, M, R, DiagLength)) {break;}

	}//End Time Steps

	cout << "Function Name:  		" << functionName << endl;
	cout << "Best Fitness:   		" << setprecision(18) << bestFitness << endl;
	cout << "Best Probe #:   		" << setprecision(0) << bestProbeNumber << endl;
	cout << "Best Time Step: 		" << bestTimeStep << endl;
	cout << "Best Gamma:			" << setprecision(2) << Gamma << endl;
	cout << "Probes Per Axis:		" << numProbesPerAxis << endl;
	cout << "Number of Probes:		" << Np << endl;
	cout << "Last Step:      		" << setprecision(0) << lastStep << endl;
	cout << "Number of Evals:       " << numEvals << endl;


	//PrintResults2(R, A, M, Np, Nd, lastStep, functionName);
}


int main() {

	vector<long double> xMin;
	vector<long double> xMax;
	int lastStep;
	int Np, Nd, Nt;
	long double Alpha, Beta, Frep, Gamma, min, max;
	long double (*rPtr)(Array3D<long double> , int, int, int) = NULL;
	CStopWatch timer;

/*
	timer.startTimer();
	rPtr = &IrisNN2;
	Nd = 35; Nt = 1000; min = -5; max = 5;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "IrisNN2 - Mini Benchmark", 1);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "IrisNN2 - CFO", 1);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;
*/
	timer.startTimer();
	rPtr = &IrisNN2;
	Nd = 35; Nt = 1000;	min = -5; max = 5;
	//MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "IrisNN2 - Mini Benchmark", 2);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	Np = 140; Gamma = 1;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "IrisNN2 - CFO", 2);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	rPtr = &IrisNN2;
	Nd = 35; Nt = 1000;	min = -5; max = 5;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "IrisNN2 - Mini Benchmark", 3);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "IrisNN2 - CFO", 3);
	timer.stopTimer();
	cout <<  "Time: " << timer.getElapsedTime() << endl;

/*
	timer.startTimer();
	rPtr = &IrisNN;
	Nd = 21; Nt = 1000; min = -5; max = 5;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "IrisNN - Mini Benchmark", 1);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "IrisNN - CFO", 1);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	rPtr = &IrisNN;
	Nd = 21; Nt = 1000;	min = -5; max = 5;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "IrisNN - Mini Benchmark", 2);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "IrisNN - CFO", 2);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	rPtr = &IrisNN;
	Nd = 21; Nt = 1000;	min = -5; max = 5;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "IrisNN - Mini Benchmark", 3);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "IrisNN - CFO", 3);
	timer.stopTimer();
	cout <<  "Time: " << timer.getElapsedTime() << endl;
*/
/*
	timer.startTimer();
	rPtr = &XORNN;
	Nd = 9; Nt = 1000;	min = -2; max = 2;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "XORNN - Mini Benchmark", 1);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "XORNN - CFO", 1);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;
	
	cout << endl;

	timer.startTimer();
	rPtr = &XORNN;
	Nd = 9; Nt = 1000;	min = -2; max = 2;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "XORNN - Mini Benchmark", 2);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;
	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "XORNN - CFO", 2);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	rPtr = &XORNN;
	Nd = 9; Nt = 1000;	min = -2; max = 2;
	MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, "XORNN - Mini Benchmark", 3);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;

	cout << endl;

	timer.startTimer();
	xMin.clear();xMax.clear();
	xMin.resize(Nd, min); xMax.resize(Nd,  max);
	Alpha = 1; Beta = 2; Frep = 0.5;
	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, "XORNN - CFO", 3);
	timer.stopTimer();
	cout << setprecision(5) << fixed << "Time: " << timer.getElapsedTime() << endl;
*/	
	return 0;
}

